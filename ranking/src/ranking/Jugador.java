package ranking;

public class Jugador implements Comparable<Jugador>{
	private String nombre;
	private int puntos;
	
	public Jugador(String pNombre,int pPuntos){
		this.nombre=pNombre;
		this.puntos=pPuntos;
	}
	
	public String getNombre(){
		return nombre;
	}
	
	public int getPuntos(){
		return puntos;
	}	
	
	 @Override
     public int compareTo(Jugador o) {
        String a=new String(String.valueOf(this.getPuntos())+this.getNombre());
        String b=new String(String.valueOf(o.getPuntos())+o.getNombre());
        return a.compareTo(b);
    }

	
	
	
}
