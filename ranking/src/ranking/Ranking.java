package ranking;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class Ranking {
	
	private static Ranking mRanking;
	private ArrayList<Jugador> ranking;
	
	private Ranking(){
		if(ranking == null){
			ranking = new ArrayList<Jugador>();
		}
	}
	
	public void cargarRanking(){
		
		String nombre;
		int puntos;
		File archivo = new File("C:/Users/anahe/Desktop/Ranking.txt");
		
		try {
			@SuppressWarnings("resource")
			Scanner entrada = new Scanner(archivo);
			String linea;
			
			while (entrada.hasNext()) {
				linea = entrada.nextLine();
				String[] prueba = linea.split(" ");
				nombre= prueba[0];
				puntos=Integer.parseInt(prueba[1]);
				
				Jugador jugador = new Jugador(nombre,puntos);
				ranking.add(jugador);
			}

		} catch (Exception e) {
			System.out.println("############Error de cargado############");

		}
	}
	
	public void actualizarRanking(String nombre,int puntos){
		
		Jugador j1= new Jugador(nombre,puntos);
		
		if (ranking.size()>= 10){
			if(ranking.get(ranking.size()-1).getPuntos()< j1.getPuntos()){
				ranking.remove(ranking.size()-1);
				ranking.add(j1);
				
			}
		}else{
			
			ranking.add(j1);
		}
		
		ordenarRanking(ranking);

	}
	
	public void ordenarRanking(ArrayList<Jugador> ranking){
		Collections.sort(ranking,Collections.reverseOrder());
	}
	
	public void escribir() {

		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			int z = 0;
			fichero = new FileWriter("C:/Users/anahe/Desktop/Ranking.txt");
			pw = new PrintWriter(fichero);
			int ultimo = ranking.size();
			while (z != ultimo) {
				pw.print(ranking.get(z).getNombre()+" "+ranking.get(z).getPuntos());
				pw.println();
				z++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					System.out.println(" ");
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public static Ranking getRanking(){
		if (mRanking == null){
			mRanking = new Ranking();
		}
		return mRanking;
		
	}
	
	public String mostrarRanking(){
		String resultado = "\t"+"\t"+"Nombre"+"\t"+"Puntuación"+"\n"+"\n";

		for (int z=0;z<ranking.size();z++){
		
			if (resultado==null){
				resultado= "\t"+"\t"+ranking.get(z).getNombre()+"\t"+ranking.get(z).getPuntos()+ "\n";
			}else{
				resultado= resultado +"\t"+"\t"+ ranking.get(z).getNombre()+"\t"+ranking.get(z).getPuntos() + "\n";
			}
		}	
		return resultado;
	}
	
	
}
