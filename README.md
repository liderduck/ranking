# README #

Parte del trabajo final de IS(Ingeniería del software). 

### ¿Que hace este repositorio? ###

* Crea una pequeña interfaces con un ranking ordenado según los datos que lee de un fichero de texto.


### ¿Como funciona este repositorio? ###

* Este programa lee un fichero .txt
* Ordena los valores segun la puntuacion que tengan.
* Los muestra en una interface que sera integrada en un programa mas grande.

### Tecnología ###

* Java

### Creador ###

* Jonathan Guijarro Garcia